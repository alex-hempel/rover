require 'rspec'
require 'rover'

describe Rover do

  context "Invalid tags and other error states" do
    before do
      @rover = Rover.new()
    end

    describe "When both StartTime and StopTime are set to pass" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "pass", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
        expect(@rover.reason).to eq('Do not use pass for both StartTime and StopTime')
      end
    end

    describe "When StartStop tag is not valid JSON" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime" "pass", "StopTime": "pass" "Strict": "none" ')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
        @rover.instance_variable_get('@reason')
        expect(@rover.reason).to eq('Invalid StartStop Tag')
      end
    end

    describe "When StartStop tag is Undefined" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', 'Undefined')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped');
        @rover.instance_variable_get('@reason')
        expect(@rover.reason).to eq('Invalid StartStop Tag')
      end
    end

    describe "When StartStop tag does not have the correct structure" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "Start": "pass", "Stop": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When StartTime is not a valid cron expression" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When StopTime is not a valid cron expression" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When StopTime contains invalid values at any position" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "17 55 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When StartTime contains invalid values at any position" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 35 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When Strict contains an invalid value" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "foo" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end
  end

  context "Strict is set to alwayson" do
    before do
      @rover = Rover.new()
    end

    describe "When Instance is stopped" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "alwayson" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When Instance is running" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "alwayson" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end
  end

  context "Strict is set to alwaysoff" do
    before do
      @rover = Rover.new()
    end

    describe "When Instance is running" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "alwaysoff" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When Instance is stopped" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "alwaysoff" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end
  end

  context "Strict is set to None" do
    before do
      @rover = Rover.new()
    end

    describe "When instance is stopped exactly at its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running exactly at its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped very close to its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 59))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running very close to its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 59))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at a time less than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 17, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at a time less than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 58, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at a time more than 15 minutes after its stop time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 20, 32, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is stopped with StartTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at or less than 15 minutes after its stop time with with StartTime pass" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 56, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running with StopTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is stopped at or less than 15 minutes after its start time with with StopTime pass" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 13, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end
 end

  context "Strict is set to All" do
    before do
      @rover = Rover.new()
    end

    describe "When instance is stopped exactly at its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * mon-fri", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running exactly at its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped very close to its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 36))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * mon-fri", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running very close to its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 36))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at a time less than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 17, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running at a time less than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 58, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at a time more than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 20, 32, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped with StartTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at or less than 15 minutes after its stop time with with StartTime pass" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 56, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running with StopTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is stopped at or less than 15 minutes after its start time with with StopTime pass" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 13, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "all" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

  end

  context "Strict is set to Shutdown" do
    before do
      @rover = Rover.new()
    end
    describe "When instance is stopped exactly at its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running exactly at its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped very close to its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 36))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running very close to its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 36))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at a time less than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 17, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at a time less than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 58, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at a time more than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 5, 03, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped with StartTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at or less than 15 minutes after its stop time with with StartTime pass" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 56, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at more than 15 minutes after its stop time with with StartTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 20, 15, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end
  end

  context "Strict is set to Startup" do
    before do
      @rover = Rover.new()
    end

    describe "When instance is stopped exactly at its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running exactly at its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped very close to its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 00, 36))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running very close to its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 59))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at a time less than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 17, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its stop time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 20, 11, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 7 * * mon-fri", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at a time less than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 58, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at a time more than 15 minutes after its stop time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 20, 32, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "55 19 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running with StopTime pass" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is stopped at or less than 15 minutes after its start time with with StopTime pass" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 13, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "pass", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end
  end

  context "Crossing Date boundaries: shut down in the morning, start at night" do
    before do
      @rover = Rover.new()
    end

    describe "When instance is stopped exactly at its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running exactly at its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 7, 55, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at a time less than 15 minutes after its start time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time with strict none" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 23, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is stopped at a time more than 15 minutes after its start time with strict startup" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 19, 23, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "startup" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running at a time less than 15 minutes after its stop time" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 7, 58, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is running at a time more than 15 minutes after its stop time with strict shutdown" do
      it 'it should stop the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 32, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 19 * * *", "StopTime": "55 7 * * *", "Strict": "shutdown" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end
  end

  context "StartTime and StopTime are within 15 minutes of each other" do
    before do
      @rover = Rover.new()
    end

    describe "When instance is stopped 12 minutes after its start time, and stop time is 10 minutes after start time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "0 8 * * *", "StopTime": "10 8 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running 12 minutes after its stop time, and start time is 10 minutes after stop time" do
      it 'it should do nothing' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 8, 12, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "10 8 * * *", "StopTime": "0 8 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('unchanged')
      end
    end

    describe "When instance is running at its stop time, and start time is 15 minutes after stop time" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 5, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "15 5 * * *", "StopTime": "0 5 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end

    describe "When instance is stopped at its start time, and start time is 15 minutes after stop time" do
      it 'it should start the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 5, 15, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "15 5 * * *", "StopTime": "0 5 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('stopped', @rover.tag)).to eq('running')
      end
    end

    describe "When instance is running at its stop time, and start time is 20 minutes after stop time" do
      it 'it should shut down the instance' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 5, 00, 00))
        @rover.instance_variable_get('@current_time')
        @rover.instance_variable_set('@tag', '{ "StartTime": "20 5 * * *", "StopTime": "0 5 * * *", "Strict": "none" }')
        @rover.instance_variable_get('@tag')
        expect(@rover.desiredstate('running', @rover.tag)).to eq('stopped')
      end
    end
  end

  context "All the little helpers" do
    before do
      @rover = Rover.new()
    end
    describe 'tolerance_time' do
      it 'adds 15 minutes to a given time' do
        time = Time.new(2015, 1, 1, 10, 00, 00)
        expect(@rover.tolerance_time(time)).to eq(Time.new(2015, 1, 1, 10, 15, 00))

        time = @rover.lastrun('0 8 * * *',Time.new(2015, 1, 1, 8, 01, 00))
        expect(@rover.tolerance_time(time)).to eq(@rover.lastrun('0 8 * * *',Time.new(2015, 1, 1, 8, 01, 00))+60*15)
      end
    end

    describe 'a_day_ago' do
      it 'returns the time exactly 24 hours ago' do
        time = Time.new(2015, 1, 2, 10, 00, 00)
        expect(@rover.a_day_ago(time)).to eq(Time.new(2015, 1, 1, 10, 00, 00))
      end
    end

    describe 'add_a_minute' do
      it 'adds one minute to a given time' do
        time = Time.new(2015, 1, 1, 10, 00, 00)
        expect(@rover.add_a_minute(time)).to eq(Time.new(2015, 1, 1, 10, 01, 00))
        expect(@rover.minute_added).to eq(true)
      end
    end

    describe 'drop_a_minute' do
      it 'subtracts one minute from a given time' do
        time = Time.new(2015, 1, 1, 10, 00, 00)
        expect(@rover.drop_a_minute(time)).to eq(Time.new(2015, 1, 1, 9, 59, 00))
        expect(@rover.minute_added).to eq(false)
      end
    end

    describe 'has_it_been_less_than_a_minute' do
      it 'it returns true if less than a minute has passed since a given time' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 10, 00, 36))
        @rover.instance_variable_get('@current_time')
        time1 = Time.new(2015, 1, 1, 10, 00, 00)
        time2 = time1 + 60
        expect(@rover.has_it_been_less_than_a_minute?(time1, time2)).to eq(true)
      end
    end

    describe 'has_it_been_less_than_a_minute' do
      it 'it returns false if more than a minute has passed since a given time' do
        @rover.instance_variable_set('@current_time', Time.new(2015, 1, 1, 10, 01, 36))
        @rover.instance_variable_get('@current_time')
        time1 = Time.new(2015, 1, 1, 10, 00, 00)
        time2 = time1 + 60
        expect(@rover.has_it_been_less_than_a_minute?(time1, time2)).to eq(false)
      end
    end

    describe 'validate_tag' do
      it 'returns false if StartStop contains invalid JSON' do
        tag = '{ "StartTime" "pass", "StopTime": "pass" "Strict": "none"'
        expect(@rover.validate_tag(tag)).to eq(false)
      end
    end

    describe 'validate_tag' do
      it 'returns true if StartStop contains valid JSON' do
        tag = '{ "StartTime": "pass", "StopTime": "55 19 * * *", "Strict": "none" }'
        expect(@rover.validate_tag(tag)).to eq(true)
      end
    end

    describe 'validate_tag' do
      it 'returns false if StartTime is invalid cron expression' do
        tag = '{ "StartTime": "0 8 *", "StopTime": "55 19 * * *", "Strict": "none" }'
        expect(@rover.validate_tag(tag)).to eq(false)
      end
    end

    describe 'validate_tag' do
      it 'returns false if StopTime is invalid cron expression' do
        tag = '{ "StartTime": "pass", "StopTime": "55 19 *", "Strict": "none" }'
        expect(@rover.validate_tag(tag)).to eq(false)
      end
    end

    describe 'validate_tag' do
      it 'returns false if Strict is not any of startup/shutdown/none/all/alwayson/alwaysoff' do
        tag = '{ "StartTime": "pass", "StopTime": "55 19 *", "Strict": "foo" }'
        expect(@rover.validate_tag(tag)).to eq(false)
      end
    end

    describe 'validate_tag' do
      it 'returns false if StartStop contains no JSON' do
        tag = 'Undefined'
        expect(@rover.validate_tag(tag)).to eq(false)
      end
    end
  end


end

