require 'json'
require 'aws-sdk'
require 'logger'
require 'parse-cron'
require 'net/smtp'
require 'OptParse'
require 'YAML'

require_relative 'lib/rover'
require_relative 'lib/ec2'

logger = Logger.new(STDOUT)
logger.level = Logger::INFO
logger.formatter = proc do |severity, datetime, progname, msg|
  date_format = datetime.strftime("%Y-%m-%d %H:%M:%S")
  if severity == "INFO" or severity == "WARN"
    "[#{date_format}]: #{msg}\n"
  else
    "[#{date_format}]: #{msg}\n"
  end
end

# CLI Options Parser
options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: run-rover-aws.rb --env --region --proxy"
  opts.separator ""
  opts.separator "AWS Connection Options:"
  opts.on('--env ENVIRONMENT', 'AWS Account') { |v| options[:env] = v }
  opts.on('--proxy PROXY', 'HTTP Proxy') { |v| options[:proxy] = v }
  opts.on('--region REGION', 'AWS Region') { |v| options[:region] = v }
end.parse!

# Read account keys from YAML file based on Environment
config = YAML::load_file(File.join(__dir__, './cfg/config.yml'))
accesskey = config.fetch(options[:env])['accesskey']
secretkey = config.fetch(options[:env])['secretkey']
ownerid = config.fetch(options[:env])['account']

Aws.config.update({
                      credentials: Aws::Credentials.new(accesskey, secretkey),
                      region: options[:region],
                      http_proxy: options[:proxy]
                  })


rover = Rover.new
begin
  instances = EC2.new.allInstances
rescue Aws::EC2::Errors::RequestLimitExceeded
  wait = 5
  logger.info("Waiting for #{wait} seconds due to exceeded request limit")
  sleep wait
  retry
end

instances.each do |instance|
  instance = instance.instances[0]
  nametag = EC2.new.getTag(instance, "Name")
  startstoptag = EC2.new.getTag(instance, "StartStop")

  puts "#{rover.current_time} #{nametag}: #{startstoptag}, #{instance.state.name}"

  if startstoptag.nil?
    startstoptag = 'Undefined'
  end

  desiredstate = rover.desiredstate(instance.state.name, startstoptag)
  if desiredstate != 'unchanged'
    logger.info("#{nametag} (#{instance.instance_id}, #{startstoptag}) is #{instance.state.name} and should be #{desiredstate} (Reason: #{rover.reason}) ")
  end
  case desiredstate
    when 'stopped'
      EC2.new.stop_instance(instance, rover.reason)
      rover.incr_stopcount


    when 'running'
      EC2.new.start_instance(instance, rover.reason)
      rover.incr_startcount
  end

  @startstoptag = nil
end



