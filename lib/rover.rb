class Rover
  require 'json'
  require 'aws-sdk'
  require 'mysql2'
  require 'logger'
  require 'parse-cron'
  attr_reader :instancestate, :desiredstate, :current_time, :tag, :startcount, :stopcount, :reason, :startups, :shutdowns, :minute_added
  attr_writer :minute_added

  def initialize
    @current_time = Time.now
    @startcount = 0
    @stopcount = 0
    @startups = ""
    @shutdowns = ""
    @minute_added = false
  end

  def startreport(message)
    @startups+=message
  end

  def stopreport(message)
    @shutdowns+=message
  end

  def instancestate(instance)

  end

  def incr_startcount
    @startcount +=1
  end

  def incr_stopcount
    @stopcount +=1
  end

  def reset_counts
    @startcount = 0
    @stopcount = 0
  end

  def tolerance_time(time)
    tolerance = time + 60*15
    tolerance.round
  end

  def a_day_ago(time)
    tolerance = time - 60*60*24
    tolerance.round
  end

  def add_a_minute(time)
    @minute_added = true
    tolerance = time + 60
    tolerance.round
  end

  def drop_a_minute(time)
    @minute_added = false
    tolerance = time - 60
    tolerance.round
  end

  def has_it_been_less_than_a_minute?(time1, time2)
    (time1..time2).cover? @current_time
  end

  def nextrun(cron_exp,time)
    if cron_exp == "pass"
      Time.new(2215, 1, 1, 8, 34, 00)
    else
      cron_parser = CronParser.new(cron_exp)
      cron_parser.next(time)
    end

  end

  def lastrun(cron_exp,time)
    if cron_exp == "pass"
      Time.new(1971, 1, 1, 8, 34, 00)
    else
    cron_parser = CronParser.new(cron_exp)
    cron_parser.last(time)
    end
  end

  def validate_tag(tag)
    begin
      tagjson = JSON.parse(tag)
    rescue JSON::ParserError
      tagvalid = false
    else
      tagvalid = true
    end

    if tagvalid
      if tagjson.has_key?('StartTime') and tagjson.has_key?('StopTime') and tagjson.has_key?('Strict')
        tagvalid = true
      else
        tagvalid = false
      end
    end

    if tagvalid
      tagvalid = validate_tag_elements(tagjson)
    end

    tagvalid
  end

  def validate_tag_elements(tag)
    begin
      if tag['StartTime'] != "pass"
        cron = CronParser.new(tag['StartTime'])
        cron.next(Time.now)
      end
      if tag['StopTime'] != "pass"
        cron = CronParser.new(tag['StopTime'])
        cron.next(Time.now)
      end
    rescue
      false
    else
      if ['startup', 'shutdown', 'none', 'all', 'alwayson', 'alwaysoff'].include?(tag['Strict'])
        true
      else
        false
      end
    end

  end

  def desiredstate(instancestate, tag)
    if validate_tag(tag) == false and instancestate == 'running'
      @reason = "Invalid StartStop Tag"
      'stopped'
    elsif validate_tag(tag) == false and instancestate == 'stopped'
      @reason = "Invalid StartStop Tag"
      'unchanged'
    else
      strict_value = JSON.parse(tag)['Strict']
      start_time = JSON.parse(tag)['StartTime']
      stop_time = JSON.parse(tag)['StopTime']
      case strict_value
        when 'alwayson'
          case instancestate
            when 'stopped'
              @reason = "AlwaysOn"
              'running'
            else
              'unchanged'
          end
        when 'alwaysoff'
          case instancestate
            when 'running'
              @reason = "AlwaysOff"
              'stopped'
            else
              'unchanged'
          end
        else
          case [start_time, stop_time]
            when ['pass', 'pass']
              case instancestate
                when 'running'
                  @reason = "Do not use pass for both StartTime and StopTime"
                  'stopped'
                when 'stopped'
                  'unchanged'
              end
            else
              case [start_time, instancestate]
                when ['pass', 'stopped']
                 'unchanged'
                else
                  case [stop_time, instancestate]
                    when ['pass', 'running']
                      'unchanged'
                    else
                      calculate_desiredstate(instancestate, tag)
                  end

              end

          end
      end
    end
  end

  def calculate_desiredstate(instancestate, tag)
    strict_value = JSON.parse(tag)['Strict']
    start_time = JSON.parse(tag)['StartTime']
    stop_time = JSON.parse(tag)['StopTime']
    @reason = "Scheduled"
    time = @current_time
    if lastrun(start_time,time) == a_day_ago(time) or lastrun(stop_time,time) == a_day_ago(time) or has_it_been_less_than_a_minute?(lastrun(start_time,time), time) or has_it_been_less_than_a_minute?(lastrun(stop_time,time), time)
      time = add_a_minute(time)
    end

    case [strict_value, instancestate]
      when ['none','stopped']
        if lastrun(start_time,time) <= time and time <= tolerance_time(lastrun(start_time,time)) and lastrun(stop_time,time) <= lastrun(start_time,time) and nextrun(stop_time,time) >= tolerance_time(lastrun(start_time,time))
          'running'
        else
          'unchanged'
        end

      when ['none','running']
        if lastrun(stop_time,time) <= time and time < tolerance_time(lastrun(stop_time,time)) and lastrun(stop_time,time) > lastrun(start_time,time) and nextrun(start_time,time) >= tolerance_time(lastrun(stop_time,time))
          'stopped'
        else
          'unchanged'
        end

      when ['startup','stopped']
        if lastrun(start_time,time) <= time and lastrun(start_time,time) < lastrun(stop_time,time)
          'unchanged'
        elsif lastrun(start_time,time) <= time and time > tolerance_time(lastrun(start_time,time))
          'running'
        elsif lastrun(start_time,time) <= time and lastrun(start_time,time) > lastrun(stop_time,time)
          'running'
        else
          'unchanged'
        end

      when ['startup','running']
        if lastrun(stop_time,time) <= time and time > tolerance_time(lastrun(stop_time,time)) and tag['StopTime'] == 'pass'
          'unchanged'
        elsif lastrun(stop_time,time) <= time and time < tolerance_time(lastrun(stop_time,time)) and instancestate == 'running'
          'stopped'
        else
          'unchanged'
        end

      when ['shutdown','stopped']
        if lastrun(start_time,time) <= time and time > tolerance_time(lastrun(start_time,time))
          'unchanged'
        elsif lastrun(start_time,time) <= time and lastrun(start_time,time) > lastrun(stop_time,time) and tag['StartTime'] != 'pass'
          'running'
        else
          'unchanged'
        end

      when ['shutdown','running']
        if lastrun(stop_time,time) <= time and time > tolerance_time(lastrun(stop_time,time)) and start_time == 'pass'
        'unchanged'
        elsif lastrun(stop_time,time) <= time and lastrun(start_time,time) < lastrun(stop_time,time) and tag['StartTime'] != 'pass'
        'stopped'
        else
          'unchanged'
        end

      when ['all','running']
        if lastrun(stop_time,time) <= time  and lastrun(start_time,time) < lastrun(stop_time,time)
            'stopped'
        else
          'unchanged'
        end

      when ['all','stopped']
        if lastrun(start_time,time) < lastrun(stop_time,time) and lastrun(start_time,time) <= time and time > tolerance_time(lastrun(start_time,time))
          'unchanged'
        elsif lastrun(start_time,time) <= time and lastrun(start_time,time) > lastrun(stop_time,time)
          'running'
        else
          'unchanged'
        end
    end
  end
end