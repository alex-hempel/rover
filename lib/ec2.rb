require 'aws-sdk-core'
require 'logger'
Aws.use_bundled_cert!

class EC2
  attr_reader :id, :timetag

  def initialize
    @waittime = Random.new
    @client = Aws::EC2::Client.new
  end

  def allInstances
    resp = (@client.describe_instances({ filters: [{ name: 'tag:StartStop', values:['*'] }]})).reservations
  end

  def getTag(instance, tagkey)
    instance.tags.each do |tag|
      return tag.value if tag.key == "#{tagkey}"
    end
  end

  def start_instance(instance, reason)
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
    @logger.formatter = proc do |severity, datetime, progname, msg|
      date_format = datetime.strftime("%Y-%m-%d %H:%M:%S")
      if severity == "INFO" or severity == "WARN"
        "[#{date_format}]: #{msg}\n"
      else
        "[#{date_format}]: #{msg}\n"
      end
    end
    @logger.info("Starting Instance #{instance.instance_id} (Reason: #{reason})")
    tries ||=3
    begin
        @client.start_instances({instance_ids: ["#{instance.instance_id}"]})
    rescue Aws::EC2::Errors::RequestLimitExceeded
      wait = @waittime.rand(5..20)
      logger.info("Waiting for #{wait} seconds due to exceeded request limit")
      sleep wait
      retry unless (tries -= 1).zero?
    end

  end

  def stop_instance(instance, reason)
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
    @logger.formatter = proc do |severity, datetime, progname, msg|
      date_format = datetime.strftime("%Y-%m-%d %H:%M:%S")
      if severity == "INFO" or severity == "WARN"
        "[#{date_format}]: #{msg}\n"
      else
        "[#{date_format}]: #{msg}\n"
      end
    end
    @logger.info("Stopping Instance #{instance.instance_id} (Reason: #{reason})")
    tries ||=3
    begin
      @client.stop_instances({instance_ids: ["#{instance.instance_id}"]})
    rescue Aws::EC2::Errors::RequestLimitExceeded
      wait = @waittime.rand(5..20)
      logger.info("Waiting for #{wait} seconds due to exceeded request limit")
      sleep wait
      retry unless (tries -= 1).zero?
    end
  end

   def instance_state
    puts "Watch this space"
  end

end