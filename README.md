# README #

Rover is a Ruby script that controls AWS EC2 instances.

Version: 2016.01.19pre

###Build Status###

TBA

### What is this repository for? ###

Rover controls AWS EC2 instances through a specially formatted resource tag. It can be used to shut down and start up instances on a schedule.

### How do I get set up? ###

* Tag your instances
* Deploy Rover
* Run Bundler
* Add an API Key
* Schedule Rover

####Tag your instances####

For Rover to act on any instances, they need to have a specific tag. The Tag key is StartStop, the tag value is a JSON string with three fields:

* StartTime - a CRON expression or 'pass'
* StopTime - a CRON expression or 'pass' 
* Strict - alwayson|alwaysoff|startup|shutdown|none|all
```json
{ "StartTime": "0 7 * * mon-fri", "StopTime": "55 20 * * *", "Strict": "none" }
```

####Deploy Rover####
Rover can run anywhere that has access to the AWS API. Simply download the ZIP file or clone the repo onto a suitable machine, and follow the instructions below.

####Run Bundler####

####Add an API Key####
Edit the file cfg/config.yml to add an API key and Account ID for your AWS account.

####Schedule Rover####
Rover should be run as follows on the command line:

ruby run_rover_aws.rb --env <environment> --region <AWS Region> (--proxy <optional proxy server>)

It can be triggered by a cron job, a scheduled task or an external scheduler. Depending on the number of instances you are controlling, the interval can be anywhere between 1 and 5 minutes. An interval of 1 minute is not recommended if you are controlling more than 100 instances.

### Contribution###

Watch this space

###To Do###

* ~~Move from Fog to aws-sdk~~
* Harden error handling for API request failures
* Add options for dealing with untagged instances
* Stub AWS API calls and create tests for them

### Who do I talk to? ###

* Alex Hempel (alex@ah-web.org)
